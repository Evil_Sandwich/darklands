package com.ads.darklands.desktop;

import com.ads.darklands.DarklandsGame;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		System.setProperty("user.name" ,"EnglishWords");
		config.width = 1000;
		config.height = 800;
		new LwjglApplication(DarklandsGame.getInstance(), config);
	}
}
