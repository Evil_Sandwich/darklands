package com.ads.darklands.Controller;

public enum ControlType {
    TILT,
    BUTTONS,
    TOUCH,
    NONE
}
