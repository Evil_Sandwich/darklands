package com.ads.darklands.Controller;

/**
 * Created by user on 3/2/2016.
 */
public class Pointer {
    private int x;
    private int y;
    private int button;

    public Pointer(int x, int y, int button) {
        this.button = button;
        setCoords(x, y);
    }

    public void setCoords(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
