package com.ads.darklands.Controller;

import com.ads.darklands.DarklandsGame;
import com.ads.darklands.Model.LevelWorld;
import com.ads.darklands.Model.Player;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static com.badlogic.gdx.Input.Keys.*;


public class PlayerController extends Touchpad implements InputProcessor {

    Body player;
    HashSet<Integer> pressedKeys;
    ArrayList<Character> printedChars;
    HashMap<Integer, Pointer> touchedPointers;
    ControlType control;
    HashSet<Integer> controlledButtons;

    public PlayerController(float deadzoneRadius, TouchpadStyle style, LevelWorld world) {
        super(deadzoneRadius, style);
        player = world.getPlayer().getBody();
        pressedKeys = new HashSet<Integer>();
        printedChars = new ArrayList<Character>();
        touchedPointers = new HashMap<Integer, Pointer>();
        control = DarklandsGame.getInstance().getControlType();
        controlledButtons = new HashSet<Integer>();
        controlledButtons.add(LEFT);
        controlledButtons.add(RIGHT);
        controlledButtons.add(UP);
        controlledButtons.add(DOWN);
    }

    public void updatePlayer() {
        switch (control) {
            case TOUCH:
                if(isTouched()) {
                    player.setLinearVelocity(getKnobPercentX()*100, player.getLinearVelocity().y);
                    //player.applyLinearImpulse(getKnobPercentX()*100, player.getLinearVelocity().y, player.getPosition().x, player.getPosition().y, true);
                }
                break;
        }
    }

    private boolean movesLeft() {
        switch (control) {
            case BUTTONS:
                return pressedKeys.contains(LEFT);
            case TOUCH:
                if (!touchedPointers.isEmpty()) {
                    Pointer p = touchedPointers.values().iterator().next();
                    if (2 * p.getX() <= Gdx.graphics.getWidth())
                        return true;
                }
                break;
        }
        return false;
    }

    private boolean movesRight() {
        switch (control) {
            case BUTTONS:
                return pressedKeys.contains(RIGHT);
            case TOUCH:
                if (!touchedPointers.isEmpty()) {
                    Pointer p = touchedPointers.values().iterator().next();
                    if (2 * p.getX() > Gdx.graphics.getWidth())
                        return true;
                }
                break;
        }
        return false;
    }

    private boolean getJumping() {
        switch (control) {
            case BUTTONS:
                return pressedKeys.contains(UP);
        }
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        return controlledButtons.contains(keycode) && pressedKeys.add(keycode);
    }

    @Override
    public boolean keyUp(int keycode) {
        return controlledButtons.contains(keycode) && pressedKeys.remove(keycode);
    }

    @Override
    public boolean keyTyped(char character) {
        return printedChars.add(character);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (!touchedPointers.containsKey(pointer)) {
            touchedPointers.put(pointer, new Pointer(screenX, screenY, button));
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (touchedPointers.containsKey(pointer)) {
            touchedPointers.remove(pointer);
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (touchedPointers.containsKey(pointer)) {
            touchedPointers.get(pointer).setCoords(screenX, screenY);
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}

