package com.ads.darklands.Controller;

/**
 * Created by user on 2/23/2016.
 */
public enum Direction {
    LEFT,
    RIGHT,
    UP,
    NONE
}