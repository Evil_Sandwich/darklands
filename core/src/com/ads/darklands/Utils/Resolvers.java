package com.ads.darklands.Utils;

import com.ads.darklands.DarklandsGame;
import com.ads.darklands.Model.Theme;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by user on 4/20/2016.
 */
public class Resolvers {

    public static TextureRegion resolveButton(String buttonName) {
        return getTexture(String.format("%s-%s-%s", currentButtonsQuality(), currentStringColor(), buttonName));
    }

    public static TextureRegion resolveColorImage(String name) {
        return getTexture(String.format("%s-%s", name, currentStringColor()));
    }
    public static TextureRegion resolveBrick(String name){
        return getTexture(String.format("%s-%s", currentTheme(), name));
    }
    public static String resolveAssetsFile(String name){
        return String.format("%s%s", (Gdx.app.getType() == Application.ApplicationType.Desktop ? "android/assets/" : ""), name);
    }
    public static FileHandle resolveAssetsHandle(String name){
        return new FileHandle(resolveAssetsFile(name));
    }

    public static TextureRegion getTexture(String textureName) {
        return DarklandsGame.getInstance().getTextureRegions().get(textureName);
    }

    public static BitmapFont getCurrentBigFont(){
        switch (Values.interfaceColor){
            case GREEN:
                return Values.fontBigGreen;
            case RED:
                return Values.fontBigRed;
            case PURPLE:
                return Values.fontBigPurple;
            case PURPLE_GREEN:
                return Values.fontPurpleBigGreen;
            case PURPLE_RED:
                return Values.fontPurpleBigRed;
            default:
                return new BitmapFont();
        }
    }

    public static BitmapFont getCurrentSmallFont(){
        switch (Values.interfaceColor){
            case GREEN:
                return Values.fontGreen;
            case RED:
                return Values.fontRed;
            case PURPLE:
                return Values.fontPurple;
            case PURPLE_RED:
                return Values.fontPurpleRed;
            case PURPLE_GREEN:
                return Values.fontPurpleGreen;
            default:
                return new BitmapFont();
        }
    }

    public static BitmapFont getCurrentFont(){
        switch (Values.fontSize){
            case BIG:
                return getCurrentBigFont();
            case SMALL:
                return getCurrentSmallFont();
            default:
                return new BitmapFont();
        }
    }

    public static String currentStringColor(){
        switch (Values.interfaceColor){
            case GREEN:
                return "green";
            case RED:
                return "red";
            case PURPLE:
                return "purple";
            case PURPLE_GREEN:
                return "purple-green";
            case PURPLE_RED:
                return "purple-red";
            default:
                return "purple";
        }
    }

    public static String currentButtonsQuality(){
        switch (Values.buttonsQuality){
            case HIGH:
                return "big";
            case MEDIUM:
                return "medium";
            case LOW:
                return "small";
            default:
                return "medium";
        }
    }

    public static Theme parse(String string){
        return Theme.valueOf(string);
    }

    public static String currentTheme(){
        switch (Values.levelTheme) {
            case NORMAL:
                return "normal";
            case BRICKS:
                return "bricks";
            case WINTER:
                return "winter";
            case SAND:
                return "sand";
            case SLIME:
                return "slime";
            default:
                return "normal";
        }
    }
}
