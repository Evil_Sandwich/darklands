package com.ads.darklands.Utils;

import com.ads.darklands.Model.Theme;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;

import static com.ads.darklands.View.InterfaceParameters.*;

/**
 * Created by user on 3/30/2016.
 */
public class Values {
    public static final float WORLD_WIDTH = 10.0f;
    public static final float WORLD_HEIGHT = 8.0f;
    public static final float SCREEN_WIDTH = Gdx.graphics.getWidth();
    public static final float SCREEN_HEIGHT = Gdx.graphics.getHeight();
    public static final float TILE_SIZE = 0.2f;
    public static float ppuX = (float) Gdx.graphics.getWidth() / Values.WORLD_WIDTH;
    public static float ppuY = (float) Gdx.graphics.getHeight() / Values.WORLD_HEIGHT;
    public static Theme levelTheme = Theme.SAND;
    public static InterfaceColor interfaceColor = InterfaceColor.RED;
    public static FontSize fontSize = FontSize.BIG;
    public static ButtonsQuality buttonsQuality = ButtonsQuality.HIGH;

    public static BitmapFont font;
    public static BitmapFont font2;
    public static BitmapFont fontRed;
    public static BitmapFont fontGreen;
    public static BitmapFont fontPurple;
    public static BitmapFont fontBigRed;
    public static BitmapFont fontBigPurple;
    public static BitmapFont fontBigGreen;
    public static BitmapFont fontPurpleRed;
    public static BitmapFont fontPurpleGreen;
    public static BitmapFont fontPurpleBigRed;
    public static BitmapFont fontPurpleBigGreen;


    public static HashMap<String, TextureRegion> textureRegions;

    public static void loadGraphics() {
        String theme;
        textureRegions = new HashMap<String, TextureRegion>();
        Texture heroes = new Texture("android/assets/texture.png");

        textureRegions.put("hero", new TextureRegion(heroes, 0, 74, 16, 17));


        textureRegions.put("outer-rim", new TextureRegion(heroes, 59, 104, 100, 100));
        textureRegions.put("inner-rim-red", new TextureRegion(heroes, 59, 273, 50, 50));
        textureRegions.put("inner-rim-purple", new TextureRegion(heroes, 59, 323, 50, 50));
        textureRegions.put("inner-rim-green", new TextureRegion(heroes, 59, 223, 50, 50));
        textureRegions.put("inner-rim-purple-red", new TextureRegion(heroes, 59, 373, 50, 50));
        textureRegions.put("inner-rim-purple-green", new TextureRegion(heroes, 59, 423, 50, 50));
        textureRegions.put("progress-bar", new TextureRegion(heroes, 185, 69, 203, 26));
        textureRegions.put("heart", new TextureRegion(heroes, 0, 278, 16, 19));
        for (int i = 0; i < 5; i++) {

            switch (i) {
                case 0:
                    theme = "normal";
                    break;
                case 1:
                    theme = "sand";
                    break;
                case 2:
                    theme = "winter";
                    break;
                case 3:
                    theme = "slime";
                    break;
                case 4:
                    theme = "bricks";
                    break;
                default:
                    theme = "normal";
                    break;
            }


            textureRegions.put(String.format("%s-left-cut", theme),
                    new TextureRegion(heroes, 2 + (i * 185), 0, 21, 21));
            textureRegions.put(String.format("%s-right-cut", theme),
                    new TextureRegion(heroes, 2 + (i * 185), 23, 21, 21));

            textureRegions.put(String.format("%s-solid-road", theme),
                    new TextureRegion(heroes, 71 + (i * 185), 0, 21, 21));
            textureRegions.put(String.format("%s-solid-brick", theme),
                    new TextureRegion(heroes, 48 + (i * 185), 23, 21, 21));

            textureRegions.put(String.format("%s-slide-down", theme),
                    new TextureRegion(heroes, 163 + (i * 185), 0, 21, 21));
            textureRegions.put(String.format("%s-slide-up", theme),
                    new TextureRegion(heroes, 140 + (i * 185), 0, 21, 21));
            textureRegions.put(String.format("%s-slide-down-bottom", theme),
                    new TextureRegion(heroes, 163 + (i * 185), 23, 21, 21));
            textureRegions.put(String.format("%s-slide-up-bottom", theme),
                    new TextureRegion(heroes, 140 + (i * 185), 23, 21, 21));
        }

        textureRegions.put("air",
                new TextureRegion(heroes, 0, 48, 1, 1));
        textureRegions.put("key", new TextureRegion(heroes, 44, 77, 21, 21));

        putButtons("android/assets/Buttons/buttons-green-big.png", "big", "green");
        putButtons("android/assets/Buttons/buttons-green-medium.png", "medium", "green");
        putButtons("android/assets/Buttons/buttons-green-small.png", "small", "green");
        putButtons("android/assets/Buttons/buttons-red-big.png", "big", "red");
        putButtons("android/assets/Buttons/buttons-red-medium.png", "medium", "red");
        putButtons("android/assets/Buttons/buttons-red-small.png", "small", "red");
        putButtons("android/assets/Buttons/buttons-purple-big.png", "big", "purple");
        putButtons("android/assets/Buttons/buttons-purple-medium.png", "medium", "purple");
        putButtons("android/assets/Buttons/buttons-purple-small.png", "small", "purple");
        putButtons("android/assets/Buttons/buttons-purple-red-big.png", "big", "purple-red");
        putButtons("android/assets/Buttons/buttons-purple-red-medium.png", "medium", "purple-red");
        putButtons("android/assets/Buttons/buttons-purple-red-small.png", "small", "purple-red");
        putButtons("android/assets/Buttons/buttons-purple-green-big.png", "big", "purple-green");
        putButtons("android/assets/Buttons/buttons-purple-green-medium.png", "medium", "purple-green");
        putButtons("android/assets/Buttons/buttons-purple-green-small.png", "small", "purple-green");
        textureRegions.put("bg-green", new TextureRegion(new Texture("android/assets/bg-green.png")));
        textureRegions.put("bg-red", new TextureRegion(new Texture("android/assets/bg-red.png")));
        textureRegions.put("bg-purple", new TextureRegion(new Texture("android/assets/bg-purple.png")));
        textureRegions.put("bg-purple-red", new TextureRegion(new Texture("android/assets/bg-purple-red.png")));
        textureRegions.put("bg-purple-green", new TextureRegion(new Texture("android/assets/bg-purple-green.png")));


    }

    private static void putButtons(String fileName, String size, String color) {
        Texture buttons = new Texture(fileName);
        buttons.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        TextureRegion[][] regions = new TextureRegion[7][7];

        int buttonSize = buttons.getWidth() * 92 / 700;
        int cellSize = buttons.getWidth() / 7;
        int cellPadding = buttons.getWidth() * 4 / 700;
        for (int i = 0; i < 7; i++)
            for (int j = 0; j < 7; j++)
                regions[i][j] = new TextureRegion(buttons, cellPadding + j * cellSize, cellPadding + i * cellSize, buttonSize, buttonSize);

        textureRegions.put(String.format("%s-%s-Button1", size, color), regions[0][0]);
        textureRegions.put(String.format("%s-%s-Button2", size, color), regions[0][1]);
        textureRegions.put(String.format("%s-%s-Button3", size, color), regions[0][2]);
        textureRegions.put(String.format("%s-%s-Button4", size, color), regions[0][3]);
        textureRegions.put(String.format("%s-%s-Button5", size, color), regions[0][4]);
        textureRegions.put(String.format("%s-%s-Button6", size, color), regions[1][0]);
        textureRegions.put(String.format("%s-%s-Button7", size, color), regions[1][1]);
        textureRegions.put(String.format("%s-%s-Button8", size, color), regions[1][2]);
        textureRegions.put(String.format("%s-%s-Button9", size, color), regions[1][3]);
        textureRegions.put(String.format("%s-%s-Button10", size, color), regions[1][4]);
        textureRegions.put(String.format("%s-%s-PlayButton", size, color), regions[2][0]);
        textureRegions.put(String.format("%s-%s-BackButton", size, color), regions[2][1]);
        textureRegions.put(String.format("%s-%s-SettingsButton", size, color), regions[2][2]);
        textureRegions.put(String.format("%s-%s-SoundOnButton", size, color), regions[2][3]);
        textureRegions.put(String.format("%s-%s-SoundOffButton", size, color), regions[2][4]);
        textureRegions.put(String.format("%s-%s-HelpButton", size, color), regions[3][0]);
        textureRegions.put(String.format("%s-%s-PauseButton", size, color), regions[3][1]);
        textureRegions.put(String.format("%s-%s-TiltButton", size, color), regions[3][2]);
        textureRegions.put(String.format("%s-%s-ResetButton", size, color), regions[3][3]);
        textureRegions.put(String.format("%s-%s-RatingButton", size, color), regions[3][4]);
        textureRegions.put(String.format("%s-%s-EmptyButton", size, color), regions[4][0]);
        textureRegions.put(String.format("%s-%s-LowButton", size, color), regions[4][1]);
        textureRegions.put(String.format("%s-%s-MediumButton", size, color), regions[4][2]);
        textureRegions.put(String.format("%s-%s-HighButton", size, color), regions[4][3]);
        textureRegions.put(String.format("%s-%s-JumpButton", size, color), regions[4][4]);
        textureRegions.put(String.format("%s-%s-AttackButton", size, color), regions[5][0]);

    }

    public static void loadFonts() {
        font2 = new BitmapFont(Resolvers.resolveAssetsHandle("Font/BigHeader.fnt"));

        fontRed = new BitmapFont(Resolvers.resolveAssetsHandle("Font/Red/RedText.fnt"));

        fontGreen = new BitmapFont(Resolvers.resolveAssetsHandle("Font/Green/GreenText.fnt"));

        fontPurple = new BitmapFont(Resolvers.resolveAssetsHandle("Font/Purple/PurpleText.fnt"));

        fontBigPurple = new BitmapFont(Resolvers.resolveAssetsHandle("Font/Purple/PurpleBigText.fnt"));

        fontBigRed = new BitmapFont(Resolvers.resolveAssetsHandle("Font/Red/RedBigText.fnt"));

        fontBigGreen = new BitmapFont(Resolvers.resolveAssetsHandle("Font/Green/GreenBigText.fnt"));

        fontPurpleRed = new BitmapFont(Resolvers.resolveAssetsHandle("Font/Purple-Red/Purple-RedText.fnt"));

        fontPurpleGreen = new BitmapFont(Resolvers.resolveAssetsHandle("Font/Purple-Green/Purple-GreenText.fnt"));

        fontPurpleBigRed = new BitmapFont(Resolvers.resolveAssetsHandle("Font/Purple-Red/Purple-RedBigText.fnt"));

        fontPurpleBigGreen = new BitmapFont(Resolvers.resolveAssetsHandle("Font/Purple-Green/Purple-GreenBigText.fnt"));


    }
}
