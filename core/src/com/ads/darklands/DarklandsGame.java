package com.ads.darklands;

import com.ads.darklands.Controller.ControlType;
import com.ads.darklands.Model.Theme;
import com.ads.darklands.Screen.HelpScreen;
import com.ads.darklands.Screen.LevelScreen;
import com.ads.darklands.Screen.LevelsScreen;
import com.ads.darklands.Screen.MainMenuScreen;
import com.ads.darklands.Screen.RatingScreen;
import com.ads.darklands.Screen.SettingsScreen;
import com.ads.darklands.Utils.Values;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;


import java.util.HashMap;

import static com.ads.darklands.Controller.ControlType.BUTTONS;
import static com.ads.darklands.Controller.ControlType.TOUCH;

/**
 * Created by user on 3/9/2016.
 */
public class DarklandsGame extends Game {

    private static DarklandsGame ourInstance = new DarklandsGame();
    private String currentColor;
    private DarklandsGame() {
    }
    public static DarklandsGame getInstance() {
        return ourInstance;
    }

    ControlType controlType;
    MainMenuScreen mainMenuScreen;
    SettingsScreen settingsScreen;
    RatingScreen ratingScreen;
    HelpScreen helpScreen;
    LevelsScreen levelsScreen;
    LevelScreen levelScreen;


    private SpriteBatch batch;


    @Override
    public void create() {
        //Values.ppuX = (float) Gdx.graphics.getWidth() / Values.WORLD_WIDTH;
        //Values.ppuY = (float) Gdx.graphics.getHeight() / Values.WORLD_HEIGHT;
        Values.loadGraphics();
        Values.loadFonts();
        batch = new SpriteBatch();

        controlType = TOUCH;
        mainMenuScreen = new MainMenuScreen(batch, Values.textureRegions);
        settingsScreen = new SettingsScreen(batch, Values.textureRegions);
        ratingScreen = new RatingScreen(batch, Values.textureRegions);
        helpScreen = new HelpScreen(batch, Values.textureRegions);
        levelsScreen = new LevelsScreen(batch, Values.textureRegions);
        levelScreen = new LevelScreen("");
        showMainMenuScreen();
    }

    public void showMainMenuScreen() {
        setScreen(mainMenuScreen);
    }

    public void showSettingsScreen() {
        setScreen(settingsScreen);
    }

    public void showRatingScreen() {
        setScreen(ratingScreen);
    }

    public void showHelpScreen() {
        setScreen(helpScreen);
    }

    public void showLevelScreen(String fileName) {
        if (fileName != null) {
            levelScreen.setFileName(fileName);
            setScreen(levelScreen);
        }
    }

    /*public void showGameScreen() {
        setScreen(gameScreen);
    }*/

    public void showLevels() {
        setScreen(levelsScreen);
    }

    /*public ControlType setControlType(){
        if (controlType = TOUCH) {
            controlType = controlType.BUTTONS;
            return controlType;
        }
        controlType = controlType.BUTTONS;
        return controlType;
    }*/

    public ControlType getControlType() {
        return controlType;
    }

    public HashMap<String, TextureRegion> getTextureRegions() {
        return Values.textureRegions;
    }

    public SpriteBatch getBatch() {
        return batch;
    }

    public void setCurrentColor(String currentColor) {
        this.currentColor = currentColor;
    }
}