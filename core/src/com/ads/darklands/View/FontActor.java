package com.ads.darklands.View;

import com.ads.darklands.Utils.Values;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by user on 3/9/2016.
 */
public class FontActor extends Actor {
    GlyphLayout layout;
    BitmapFont font;
    String str;

    public FontActor(String str, float x, float y, BitmapFont font) {
        setPosition(x * Values.ppuX, y * Values.ppuY);
        this.font = font;
        this.str = str;
        this.layout = new GlyphLayout(font, str);
    }
    public void setFont(BitmapFont font){
        this.font = font;
        layout = new GlyphLayout(font, str);
    }

    public void setToHorizontalCenter(){
        setPosition((Values.SCREEN_WIDTH-layout.width)/2f, getY());
    }
    public void setToVerticalCenter(){
        setPosition(getX(), (Values.SCREEN_HEIGHT-layout.height)/2f);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        font.draw(batch, layout, getX(), getY());
    }
}
