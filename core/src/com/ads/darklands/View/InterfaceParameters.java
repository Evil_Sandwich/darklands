package com.ads.darklands.View;

/**
 * Created by user on 5/25/2016.
 */
public class InterfaceParameters{
    public enum InterfaceColor {
        GREEN,
        PURPLE,
        RED,
        PURPLE_RED,
        PURPLE_GREEN
    }
    public enum FontColor {
        GREEN,
        PURPLE,
        RED
    }
    public enum FontSize{
        BIG,
        MEDIUM,
        SMALL
    }
    public enum ButtonsQuality{
        HIGH,
        MEDIUM,
        LOW
    }
    public enum FontType {
        Header,
        SimpleText,
        FontGreen,
        FontRed,
        FontPurple
    }
}
