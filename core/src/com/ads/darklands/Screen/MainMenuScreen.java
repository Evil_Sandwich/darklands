package com.ads.darklands.Screen;


import com.ads.darklands.DarklandsGame;
import com.ads.darklands.Utils.Resolvers;
import com.ads.darklands.View.FontActor;
import com.ads.darklands.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;

/**
 * Created by user on 1/25/2016.
 */
public class MainMenuScreen implements Screen {
    private ImageActor backGround;
    private ImageActor playButton;
    private ImageActor toSettingsButton;
    private ImageActor ratingButton;
    private ImageActor helpButton;
    private Stage stage;
    private FontActor mainMenuText;

    public MainMenuScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        mainMenuText = new FontActor("DARKLANDS", 5.0f, 7.5f, Resolvers.getCurrentBigFont());
        mainMenuText.setToHorizontalCenter();

        backGround = new ImageActor(Resolvers.resolveColorImage("bg"), 0f, 0f, 10f, 8f);

        playButton = new ImageActor(Resolvers.resolveButton("PlayButton"), 3.75f, 3.5f, 2.5f, 2.5f);
        playButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                DarklandsGame.getInstance().showLevels();
            }
        });
        toSettingsButton = new ImageActor(Resolvers.resolveButton("SettingsButton"), 0.5f, 0.5f, 2.5f, 2.5f);
        toSettingsButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                DarklandsGame.getInstance().showSettingsScreen();
            }
        });
        ratingButton = new ImageActor(Resolvers.resolveButton("RatingButton"), 3.75f, 0.5f, 2.5f, 2.5f);
        ratingButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                DarklandsGame.getInstance().showRatingScreen();
            }
        });
        helpButton = new ImageActor(Resolvers.resolveButton("HelpButton"), 7.0f, 0.5f, 2.5f, 2.5f);
        helpButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                DarklandsGame.getInstance().showHelpScreen();
            }
        });


        stage.addActor(backGround);
        stage.addActor(playButton);
        stage.addActor(helpButton);
        stage.addActor(ratingButton);
        stage.addActor(toSettingsButton);
        stage.addActor(mainMenuText);

    }

    @Override
    public void show() {
        backGround.setImage(Resolvers.resolveColorImage("bg"));
        playButton.setImage(Resolvers.resolveButton("PlayButton"));
        toSettingsButton.setImage(Resolvers.resolveButton("SettingsButton"));
        ratingButton.setImage(Resolvers.resolveButton("RatingButton"));
        helpButton.setImage(Resolvers.resolveButton("HelpButton"));
        mainMenuText.setFont(Resolvers.getCurrentBigFont());
        mainMenuText.setToHorizontalCenter();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }


}



