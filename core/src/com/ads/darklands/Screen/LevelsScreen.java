package com.ads.darklands.Screen;

//import com.ads.arcanoid.Controller.GoToGame;

import com.ads.darklands.DarklandsGame;
import com.ads.darklands.Utils.Resolvers;
import com.ads.darklands.Utils.Values;
import com.ads.darklands.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;

/**
 * Created by user on 2/8/2016.
 */
public class LevelsScreen implements Screen {
    private ImageActor helpBackGround;
    private ImageActor backToMainMenu;
    private ImageActor[] pickLevelButtons;
    private ImageActor[] pickLevelButtons1;
    private Stage stage;

    public LevelsScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        helpBackGround = new ImageActor(Resolvers.resolveColorImage("bg"), 0f, 0f, 10f, 8f);
        backToMainMenu = new ImageActor(Resolvers.resolveButton("BackButton"), 0f, 7f, 1f, 1f);
        backToMainMenu.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                DarklandsGame.getInstance().showMainMenuScreen();
            }
        });
        stage.addActor(helpBackGround);
        stage.addActor(backToMainMenu);

        pickLevelButtons = new ImageActor[5];
        pickLevelButtons1 = new ImageActor[5];
        for (int i = 0; i < pickLevelButtons.length; i++) {
            pickLevelButtons[i] = new ImageActor(Resolvers.resolveButton(String.format("Button%d", i + 1)), ((Values.WORLD_WIDTH / 5f) * i) + 0.5f, 6.0f, 1f, 1f);
            pickLevelButtons[i].addListener(new LevelTraveler(String.format("%d.txt", i+1)));
            stage.addActor(pickLevelButtons[i]);
            pickLevelButtons1[i] = new ImageActor(Resolvers.resolveButton(String.format("Button%d", i + 6)), ((Values.WORLD_WIDTH / 5f) * i) + 0.5f, 4.0f, 1f, 1f);
            pickLevelButtons1[i].addListener(new LevelTraveler(String.format("%d.txt", i+6)));
            stage.addActor(pickLevelButtons1[i]);
        }

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        helpBackGround.setImage(Resolvers.resolveColorImage("bg"));
        backToMainMenu.setImage(Resolvers.resolveButton("BackButton"));
        for (int i = 0; i < pickLevelButtons.length; i++) {
            pickLevelButtons[i].setImage(Resolvers.resolveButton(String.format("Button%d", i + 1)));
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();

    }

    class LevelTraveler extends ClickListener{
        String fileName;
        public LevelTraveler(String fileName){
            this.fileName = fileName;
        }
        public void clicked(InputEvent event, float x, float y) {
            DarklandsGame.getInstance().showLevelScreen(Resolvers.resolveAssetsFile(fileName));
        }
    }

}


