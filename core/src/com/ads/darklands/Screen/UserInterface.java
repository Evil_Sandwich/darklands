package com.ads.darklands.Screen;

import com.ads.darklands.Controller.PlayerController;
import com.ads.darklands.DarklandsGame;
import com.ads.darklands.Model.LevelWorld;
import com.ads.darklands.Model.MyContactListener;
import com.ads.darklands.Model.Player;
import com.ads.darklands.Utils.Resolvers;
import com.ads.darklands.Utils.Values;
import com.ads.darklands.View.ImageActor;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by user on 6/3/2016.
 */
public class UserInterface extends Stage {
    private PlayerController controller;
    private ImageActor pauseButton;
    private ImageActor attackButton;
    private ImageActor jumpButton;
    private ImageActor progressBar;
    private ImageActor heart1;
    private ImageActor heart2;
    private ImageActor heart3;
    private Vector2 jumpImpulse;
    private Body player;
    private Player playerActor;

    public UserInterface(Viewport viewport, Batch batch, LevelWorld levelWorld) {
        super(viewport, batch);
        player = levelWorld.getPlayer().getBody();
        playerActor = levelWorld.getPlayer();
        jumpImpulse = new Vector2(0.0f, 10000.0f);
        controller = new PlayerController(0, new Touchpad.TouchpadStyle(new TextureRegionDrawable(Values.textureRegions.get("outer-rim")), new TextureRegionDrawable(Resolvers.resolveColorImage("inner-rim"))), levelWorld);
        controller.setPosition(20, 20);
        pauseButton = new ImageActor(Resolvers.resolveButton("PauseButton"), 0, 7.0f, 1f, 1f);
        pauseButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                DarklandsGame.getInstance().showLevels();
            }
        });
        progressBar = new ImageActor(Values.textureRegions.get("progress-bar"), 4f, 0.2f, 3.0f, 0.3f);
        heart1 = new ImageActor(Values.textureRegions.get("heart"), 9.3f, 7.8f, 0.2f, 0.2f);
        heart2 = new ImageActor(Values.textureRegions.get("heart"), 9.5f, 7.8f, 0.2f, 0.2f);
        heart3 = new ImageActor(Values.textureRegions.get("heart"), 9.7f, 7.8f, 0.2f, 0.2f);
        attackButton = new ImageActor(Resolvers.resolveButton("AttackButton"), 9.0f, 1.2f, 1f, 1f);
        jumpButton = new ImageActor(Resolvers.resolveButton("JumpButton"), Values.WORLD_WIDTH - 1.0f, 0, 1f, 1f);
        jumpButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                //System.out.println("click");
                //player.applyLinearImpulse(0f, 10.0f, player.getPosition().x, player.getPosition().y, true);
                //player.setLinearVelocity(0f, 100f);
                //player.applyAngularImpulse(200f, true);
                if (playerActor.getCanJump())
                    player.applyLinearImpulse(jumpImpulse, player.getPosition(), true);
            }
        });

        addActor(pauseButton);
        addActor(attackButton);
        addActor(heart1);
        addActor(heart2);
        addActor(heart3);
        addActor(jumpButton);
        addActor(controller);
        addActor(progressBar);

    }

    public void act(float delta) {
        super.act(delta);
        controller.updatePlayer();
    }
}
