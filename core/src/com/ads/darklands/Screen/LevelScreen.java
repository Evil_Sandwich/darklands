package com.ads.darklands.Screen;

import com.ads.darklands.Controller.PlayerController;
import com.ads.darklands.DarklandsGame;
import com.ads.darklands.Model.LevelWorld;
import com.ads.darklands.Utils.Resolvers;
import com.ads.darklands.Utils.Values;
import com.ads.darklands.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by user on 3/2/2016.
 */
public class LevelScreen implements Screen {
    public LevelWorld levelWorld;
    public UserInterface userInterface;
    SpriteBatch batch;
    OrthographicCamera camera;
    HashMap<String, TextureRegion> textureRegions;
    private String fileName;
    private InputMultiplexer multiplexer;
    private Box2DDebugRenderer debugRenderer;
    private int fps;

    public LevelScreen(String fileName) {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        textureRegions = DarklandsGame.getInstance().getTextureRegions();
        batch = DarklandsGame.getInstance().getBatch();
        debugRenderer = new Box2DDebugRenderer();
        setFileName(fileName);
        Timer timer = new Timer();
        timer.schedule(new showFps(),0,1000);
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public class showFps extends TimerTask{
        @Override
        public void run() {
            System.out.println(fps);
            fps = 0;
        }
    }

    @Override
    public void show() {
        if (levelWorld != null) levelWorld.dispose();
        try {
            levelWorld = new LevelWorld(new ScreenViewport(camera), batch, textureRegions, fileName);
            OrthographicCamera newCamera = new OrthographicCamera();
            newCamera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

            userInterface = new UserInterface(new ScreenViewport(newCamera), batch, levelWorld);

            multiplexer = new InputMultiplexer();
            multiplexer.addProcessor(userInterface);
            multiplexer.addProcessor(levelWorld);
            Gdx.input.setInputProcessor(multiplexer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        levelWorld.act(delta);
        userInterface.act(delta);
        levelWorld.draw();
        userInterface.draw();
        fps++;
        debugRenderer.render(levelWorld.getWorld(), camera.combined);
    }

    @Override
    public void resize(int width, int height) {
        levelWorld.getViewport().update(width, height, true);
        userInterface.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        levelWorld.dispose();
    }
}
