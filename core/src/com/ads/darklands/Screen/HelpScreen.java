package com.ads.darklands.Screen;


import com.ads.darklands.DarklandsGame;
import com.ads.darklands.Utils.Resolvers;
import com.ads.darklands.View.FontActor;
import com.ads.darklands.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;

/**
 * Created by user on 1/26/2016.
 */
public class HelpScreen implements Screen {
    private ImageActor helpBackGround;
    private ImageActor backToMainMenu;
    private FontActor helpText;
    private FontActor helpText1;
    private FontActor helpText2;
    private FontActor helpText3;
    private Stage stage;

    public HelpScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        helpBackGround = new ImageActor(Resolvers.resolveColorImage("bg"), 0f, 0f, 10f, 8f);
        backToMainMenu = new ImageActor(Resolvers.resolveButton("BackButton"), 0.5f, 6.5f, 1f, 1f);
        backToMainMenu.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                DarklandsGame.getInstance().showMainMenuScreen();
            }
        });
        helpText = new FontActor("You are playing as this man", 1.5f, 6f, Resolvers.getCurrentFont());
        helpText1 = new FontActor("Your goal is to find a key ", 1.5f, 4.5f, Resolvers.getCurrentFont());
        helpText2 = new FontActor("As you pass through levels,", 1.5f, 3f, Resolvers.getCurrentFont());
        helpText3 = new FontActor("your surroundings will change", 1f, 1.5f, Resolvers.getCurrentFont());


        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);
        stage.addActor(helpBackGround);
        stage.addActor(backToMainMenu);
        stage.addActor(helpText);
        stage.addActor(helpText1);
        stage.addActor(helpText2);
        stage.addActor(helpText3);

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        helpBackGround.setImage(Resolvers.resolveColorImage("bg"));
        backToMainMenu.setImage(Resolvers.resolveButton("BackButton"));
        helpText.setFont(Resolvers.getCurrentFont());
        helpText1.setFont(Resolvers.getCurrentFont());
        helpText2.setFont(Resolvers.getCurrentFont());
        helpText3.setFont(Resolvers.getCurrentFont());

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();

    }


}

