package com.ads.darklands.Screen;

import com.ads.darklands.DarklandsGame;
import com.ads.darklands.Utils.Resolvers;
import com.ads.darklands.Utils.Values;
import com.ads.darklands.View.FontActor;
import com.ads.darklands.View.ImageActor;
import com.ads.darklands.View.InterfaceParameters;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;

import static com.ads.darklands.View.InterfaceParameters.ButtonsQuality.HIGH;
import static com.ads.darklands.View.InterfaceParameters.ButtonsQuality.LOW;
import static com.ads.darklands.View.InterfaceParameters.ButtonsQuality.MEDIUM;
import static com.ads.darklands.View.InterfaceParameters.InterfaceColor.GREEN;
import static com.ads.darklands.View.InterfaceParameters.InterfaceColor.PURPLE;
import static com.ads.darklands.View.InterfaceParameters.InterfaceColor.PURPLE_GREEN;
import static com.ads.darklands.View.InterfaceParameters.InterfaceColor.PURPLE_RED;
import static com.ads.darklands.View.InterfaceParameters.InterfaceColor.RED;

/**
 * Created by user on 1/25/2016.
 */
public class SettingsScreen implements Screen {
    private ImageActor settingsBackGround;
    private ImageActor musicButton;
    private ImageActor moveToMenuButton;
    private ImageActor tiltButton;
    private ImageActor resetButton;
    private FontActor color;
    private FontActor size;
    private Stage stage;
    private ImageActor tmp2;
    private ImageActor tmp3;
    private ImageActor tmp4;

    public SettingsScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        settingsBackGround = new ImageActor(Resolvers.resolveColorImage("bg"), 0f, 0f, 10f, 8f);
        moveToMenuButton = new ImageActor(Resolvers.resolveButton("BackButton"), 0.5f, 6.5f, 1f, 1f);
        moveToMenuButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                DarklandsGame.getInstance().showMainMenuScreen();
            }
        });
        musicButton = new ImageActor(Resolvers.resolveButton("SoundOnButton"), 1f, 3f, 2.5f, 2.5f);
        tiltButton = new ImageActor(Resolvers.resolveButton("TiltButton"), 4f, 3f, 2.5f, 2.5f);
        resetButton = new ImageActor(Resolvers.resolveButton("ResetButton"), 7f, 3f, 2.5f, 2.5f);

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);
        stage.addActor(settingsBackGround);
        stage.addActor(musicButton);
        stage.addActor(tiltButton);
        stage.addActor(resetButton);
        stage.addActor(moveToMenuButton);
        ImageActor tmp;
        tmp = new ImageActor(textureRegions.get("small-green-EmptyButton"), 2f, 1.5f, 1f, 1f);
        tmp.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Values.interfaceColor = GREEN;
                show();
            }
        });
        stage.addActor(tmp);
        tmp = new ImageActor(textureRegions.get("small-red-EmptyButton"), 3f, 1.5f, 1f, 1f);
        tmp.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Values.interfaceColor = RED;
                show();
            }
        });
        stage.addActor(tmp);
        tmp = new ImageActor(textureRegions.get("small-purple-EmptyButton"), 4f, 1.5f, 1f, 1f);
        tmp.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Values.interfaceColor = PURPLE;
                show();
            }
        });
        stage.addActor(tmp);
        tmp = new ImageActor(textureRegions.get("small-purple-green-Button1") , 2.5f , 0.5f , 1f , 1f);
        tmp.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Values.interfaceColor = PURPLE_GREEN;
                show();
            }
                        });
        stage.addActor(tmp);
        tmp = new ImageActor(textureRegions.get("small-purple-red-Button1") , 3.5f , 0.5f , 1f , 1f);
        tmp.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Values.interfaceColor = PURPLE_RED;
                show();
            }
        });
        stage.addActor(tmp);

        color = new FontActor("COLOR", 3f, 0f, Resolvers.getCurrentFont());
        stage.addActor(color);

        tmp2 = new ImageActor(Resolvers.resolveButton("LowButton"), 6f, 1.5f, 1f, 1f);
        tmp2.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Values.buttonsQuality = LOW;
                show();
            }
        });
        stage.addActor(tmp2);
        tmp3 = new ImageActor(Resolvers.resolveButton("MediumButton"), 7f, 1.5f, 1f, 1f);
        tmp3.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Values.buttonsQuality = MEDIUM;
                show();
            }
        });
        stage.addActor(tmp3);
        tmp4 = new ImageActor(Resolvers.resolveButton("HighButton") , 8f , 1.5f , 1f , 1f);
        tmp4.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Values.buttonsQuality = HIGH;
            show();
            }
        });
        stage.addActor(tmp4);
        size = new FontActor("QUALITY" , 7f , 1f , Resolvers.getCurrentFont());
        stage.addActor(size);
    }


    @Override
    public void show() {
        settingsBackGround.setImage(Resolvers.resolveColorImage("bg"));
        musicButton.setImage(Resolvers.resolveButton("SoundOnButton"));
        tiltButton.setImage(Resolvers.resolveButton("TiltButton"));
        resetButton.setImage(Resolvers.resolveButton("ResetButton"));
        moveToMenuButton.setImage(Resolvers.resolveButton("BackButton"));
        tmp2.setImage(Resolvers.resolveButton("LowButton"));
        tmp3.setImage(Resolvers.resolveButton("MediumButton"));
        tmp4.setImage(Resolvers.resolveButton("HighButton"));
        color.setFont(Resolvers.getCurrentFont());
        size.setFont(Resolvers.getCurrentFont());
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

}

