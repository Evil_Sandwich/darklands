package com.ads.darklands.Screen;


import com.ads.darklands.DarklandsGame;
import com.ads.darklands.Utils.Resolvers;
import com.ads.darklands.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;

/**
 * Created by user on 1/26/2016.
 */
public class RatingScreen implements Screen {
    private ImageActor ratingBackGround;
    private ImageActor backToMainMenu;
    private Stage stage;

    public RatingScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        ratingBackGround = new ImageActor(Resolvers.resolveColorImage("bg"), 0f, 0f, 10f, 8f);
        backToMainMenu = new ImageActor(Resolvers.resolveButton("BackButton"), 0.5f, 6.5f, 1f, 1f);
        backToMainMenu.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                DarklandsGame.getInstance().showMainMenuScreen();
            }
        });


        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);
        stage.addActor(ratingBackGround);
        stage.addActor(backToMainMenu);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        ratingBackGround.setImage(Resolvers.resolveColorImage("bg"));
        backToMainMenu.setImage(Resolvers.resolveButton("BackButton"));
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();

    }

}

