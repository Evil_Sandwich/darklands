package com.ads.darklands.Model;

/**
 * Created by user on 5/15/2016.
 */
public enum Theme {
    NORMAL,
    WINTER,
    SAND,
    SLIME,
    BRICKS;
}
