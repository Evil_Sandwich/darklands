package com.ads.darklands.Model;

import com.ads.darklands.DarklandsGame;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by user on 6/8/2016.
 */
public class MyContactListener implements ContactListener {
    private Fixture A, B;

    @Override
    public void endContact(Contact contact) {
        resetFixtures(contact);
        if (getActorName(A)=="player" && getActorName(B)=="brick" || getActorName(A)=="brick" && getActorName(B)=="player") {
            System.out.println("Player ends contact");
            if (getActorName(A) == "player")
                playerFromFixture(A).setCanJump(false);
            else
                playerFromFixture(B).setCanJump(false);

        }
    }

    @Override
    public void beginContact(Contact contact) {
        resetFixtures(contact);
        if(getActorName(A)=="player" && getActorName(B) == "key" || getActorName(A) == "key" && getActorName(B) == "player") {
            System.out.println("key contact");
            DarklandsGame.getInstance().showLevels();
        }
        if (getActorName(A)=="player" && getActorName(B)=="brick" || getActorName(A)=="brick" && getActorName(B)=="player") {
            System.out.println("Player begins contact");
            if (getActorName(A) == "player")
                playerFromFixture(A).setCanJump(true);
            else
                playerFromFixture(B).setCanJump(true);

        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        /*
        resetFixtures(contact);
        if (getActorName(A) == "player" && getActorName(B) == "brick")
            playerFromFixture(A).setCanJump(true);
        if (getActorName(B) == "player" && getActorName(A) == "brick")
            playerFromFixture(B).setCanJump(true);*/

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        /*resetFixtures(contact);
        if (getActorName(A) == "player" && getActorName(B) == "brick")
            playerFromFixture(A).setCanJump(false);
        if (getActorName(B) == "player" && getActorName(A) == "brick")
            playerFromFixture(B).setCanJump(false);*/
    }

    private Player playerFromFixture(Fixture fixture){
        return (Player)fixture.getUserData();
    }
    private Actor actorFromFixture(Fixture fixture){
        return (Actor)fixture.getUserData();
    }
    private String getActorName(Fixture fixture){
        return actorFromFixture(fixture).getName();
    }

    private void resetFixtures(Contact contact){
        A = contact.getFixtureA();
        B = contact.getFixtureB();
    }

}
