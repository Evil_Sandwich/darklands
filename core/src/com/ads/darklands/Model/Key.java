package com.ads.darklands.Model;

import com.ads.darklands.View.ImageActor;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import static com.badlogic.gdx.physics.box2d.BodyDef.*;
import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.*;

/**
 * Created by user on 6/8/2016.
 */
public class Key extends ImageActor {
    Body physicsBody;

    public Key(TextureRegion img, float x, float y, float width, float height, World world) {
        super(img, x, y, width, height);
        setName("key");
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.fixedRotation = true;
        bodyDef.position.set(getX(), getY());
        FixtureDef fixtureDef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(getWidth()/2);
        shape.setPosition(new Vector2(getWidth()/2,getHeight()/2));
        fixtureDef.shape = shape;
        fixtureDef.density = 0.1f;
        fixtureDef.friction = 1.0f;
        physicsBody = world.createBody(bodyDef);
        physicsBody.createFixture(fixtureDef);
        physicsBody.getFixtureList().get(0).setUserData(this);
    }
}
