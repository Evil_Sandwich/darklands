package com.ads.darklands.Model;

import com.ads.darklands.Controller.Direction;
import com.ads.darklands.DarklandsGame;
import com.ads.darklands.Utils.Values;
import com.ads.darklands.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Timer;

/**
 * Created by user on 2/23/2016.
 */
public class Player extends Actor {
    protected TextureRegion img;

    private Direction direction;
    Timer timer;
    private Rectangle rectangle;
    private float scale;
    private boolean jumping;
    private Vector2 velocity;
    private Vector2 maxVelocity;
    private Vector2 velocityStep;
    private Vector2 velocityStop;
    private Body physicsBody;
    private boolean canJump;

    public Player(TextureRegion img, float x, float y, float maxVelocityX, float maxVelocityY, World world) {
        this.img = img;
        setName("player");
        setPosition(x * Values.ppuX, y * Values.ppuY);
        setSize(Values.TILE_SIZE * Values.ppuX, Values.TILE_SIZE * Values.ppuY);
        velocity = new Vector2(0, 0);
        jumping = false;
        maxVelocity = new Vector2(maxVelocityX, maxVelocityY);
        velocityStep = new Vector2(maxVelocityX / 30, maxVelocityY / 30);
        velocityStop = new Vector2(maxVelocityX / 10, maxVelocityY / 10);
        direction = Direction.NONE;

        //Timer timer = new Timer();
        //timer.scheduleTask(new Timer.Task() {
       //     @Override
         //   public void run() {
           //     jumping = false;
           // }
      //  }, 2);

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.fixedRotation = true;
        bodyDef.position.set(getX(), getY());
        physicsBody = world.createBody(bodyDef);
        FixtureDef fixtureDef = new FixtureDef();
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(getWidth()/2);
        circleShape.setPosition(new Vector2(getWidth()/2, getWidth()/2));
        fixtureDef.shape = circleShape;
//        fixtureDef.shape = new CircleShape();
//        fixtureDef.shape.setRadius(getWidth() / 2);
        fixtureDef.density = 0.01f;
        fixtureDef.friction = 0.1f;
        fixtureDef.restitution = 0.0f;
        physicsBody.createFixture(fixtureDef);
        //physicsBody.setMassData(new MassData());
        physicsBody.getMassData().mass = 0.1f;
        physicsBody.getFixtureList().get(0).setUserData(this);
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void act(float delta) {
        setPosition(physicsBody.getPosition().x, physicsBody.getPosition().y);
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
        /*
        batch.end();
        ShapeRenderer shape = new ShapeRenderer();
        shape.setProjectionMatrix(batch.getProjectionMatrix());
        shape.begin(ShapeRenderer.ShapeType.Line);
        shape.rect(getX() - 2, getY() - 2, getWidth() + 4, getHeight() + 4);
        shape.end();
        batch.begin();
        */
    }

    public Rectangle getRectangle() {
        return new Rectangle(getX(), getY(), getWidth() + 1, getHeight() + 1);
    }

    public void isJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public Body getBody() {
        return physicsBody;
    }

    public void setCanJump(boolean canJump) {
        this.canJump = canJump;
    }
    public boolean getCanJump() {
        return this.canJump;
    }
}
