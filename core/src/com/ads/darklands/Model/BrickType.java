package com.ads.darklands.Model;

/**
 * Created by user on 3/26/2016.
 */
public enum BrickType {
    FULL,
    UP,
    DOWN,
    RIGHT_CUT,
    LEFT_CUT,
    NONE;
}
