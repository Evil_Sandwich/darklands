package com.ads.darklands.Model;

import com.ads.darklands.DarklandsGame;
import com.ads.darklands.Utils.Resolvers;
import com.ads.darklands.Utils.Values;
import com.ads.darklands.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;


public class LevelWorld extends Stage {
    public World world;
    public Player player;
    Timer timer;


    public LevelWorld(ScreenViewport screenViewport, SpriteBatch batch, HashMap<String, TextureRegion> textureRegions, String fileName) throws
            FileNotFoundException {
        super(screenViewport, batch);
        world = new World(new Vector2(0, -100), true);//определен мир с гравитацией, направленной вниз
        world.setContactListener(new MyContactListener());

        //backGround = new ImageActor(textureRegions.get("background"), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        //backGround = new ImageActor(textureRegions.get("air"), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        //addActor(backGround);

        timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                Meteor m = new Meteor(
                        DarklandsGame.getInstance().getTextureRegions().get("heart"),5.0f,8.0f,15,0.25f,0.25f,1f);
                addActor(m);


            }
        }, 1, 4);


        int[][] field = initField(fileName);

        initType(fileName);

        ImageActor tempActor;
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                switch (field[i][j]) {
                    case 1:
                        tempActor = new Bricks(Resolvers.resolveBrick("solid-brick"), j * Values.TILE_SIZE, i * Values.TILE_SIZE, Values.TILE_SIZE, Values.TILE_SIZE, world, BrickType.FULL);
                        break;
                    case 2:
                        tempActor = new Bricks(Resolvers.resolveBrick("right-cut"), j * Values.TILE_SIZE, i * Values.TILE_SIZE, Values.TILE_SIZE, Values.TILE_SIZE, world, BrickType.RIGHT_CUT);
                        break;
                    case 3:
                        tempActor = new Bricks(Resolvers.resolveBrick("solid-road"), j * Values.TILE_SIZE, i * Values.TILE_SIZE, Values.TILE_SIZE, Values.TILE_SIZE, world, BrickType.FULL);
                        break;
                    case 4:
                        tempActor = new Bricks(Resolvers.resolveBrick("slide-down"), j * Values.TILE_SIZE, i * Values.TILE_SIZE, Values.TILE_SIZE, Values.TILE_SIZE, world, BrickType.DOWN);
                        break;
                    case 5:
                        tempActor = new Bricks(Resolvers.resolveBrick("slide-up"), j * Values.TILE_SIZE, i * Values.TILE_SIZE, Values.TILE_SIZE, Values.TILE_SIZE, world, BrickType.UP);
                        break;
                    case 6:
                        tempActor = new Bricks(Resolvers.resolveBrick("left-cut"), j * Values.TILE_SIZE, i * Values.TILE_SIZE, Values.TILE_SIZE, Values.TILE_SIZE, world, BrickType.LEFT_CUT);
                        break;
                    case 7:
                        tempActor = new Bricks(Resolvers.resolveBrick("slide-down-bottom"), j * Values.TILE_SIZE, i * Values.TILE_SIZE, Values.TILE_SIZE, Values.TILE_SIZE, world, BrickType.FULL);
                        break;
                    case 8:
                        tempActor = new Bricks(Resolvers.resolveBrick("slide-up-bottom"), j * Values.TILE_SIZE, i * Values.TILE_SIZE, Values.TILE_SIZE, Values.TILE_SIZE, world, BrickType.FULL);
                        break;
                    case 9:
                        tempActor = new Key(textureRegions.get("key"), j * Values.TILE_SIZE, i * Values.TILE_SIZE, Values.TILE_SIZE, Values.TILE_SIZE, world);
                        break;
                    default:
                        tempActor = new Bricks(textureRegions.get("air"), j * Values.TILE_SIZE, i * Values.TILE_SIZE, Values.TILE_SIZE, Values.TILE_SIZE);
                        break;
                }
                addActor(tempActor);
            }
        }

        player = new Player(textureRegions.get("hero"), 4.0f, 5.0f, 200, 100, world);
        addActor(player);

    }

    @Override
    public void act(float delta) {
        super.act(delta);
        world.step(delta, 3, 3);
    }

    private void initType(String fileName) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(fileName));
        Values.levelTheme = Resolvers.parse(sc.nextLine());
        sc.close();
    }

    //считает количество строк в файле
    private int countRows(String fileName) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(fileName));
        int result = 0;
        sc.nextLine();
        while (sc.hasNext()) {
            sc.nextLine();
            result++;
        }
        sc.close();
        return result;
    }

    //считает количество столбцов в файле
    private int countColumns(String fileName) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(fileName));
        int temp;
        int result = 0;
        sc.nextLine();
        while (sc.hasNext()) {
            temp = sc.nextLine().length();
            result = temp > result ? temp : result;
        }
        sc.close();
        return result;
    }

    //создает массив из файла
    private int[][] initField(String fileName) throws FileNotFoundException {
        int rows = countRows(fileName), columns = countColumns(fileName);
        int[][] result = new int[rows][columns];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++)
                result[i][j] = 0;
        Scanner sc = new Scanner(new File(fileName));
        String temp;
        int tmpRow = 0;
        sc.nextLine();
        while (sc.hasNext()) {
            temp = sc.nextLine();
            for (int i = 0; i < temp.length(); i++)
                result[tmpRow][i] = temp.charAt(i) - '0';
            tmpRow++;
        }
        sc.close();
        int[] tmpArr;
        for (int i = 0; i < rows / 2; i++) {
            tmpArr = result[i];
            result[i] = result[rows - 1 - i];
            result[rows - 1 - i] = tmpArr;
        }
        return result;

    }

    public Player getPlayer() {
        return player;
    }

    /*public void act (float delta) {
        super.act(delta);
            if ((player.getRectangle()))
                ArcanoidGame.getInstance().showLevels();

    }*/
    public World getWorld() {
        return world;
    }
}

