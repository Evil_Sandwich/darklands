package com.ads.darklands.Model;

import com.ads.darklands.View.ImageActor;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by user on 6/1/2016.
 */
public class Meteor extends ImageActor {
    private float scale;
    private Vector2 velocity;
    private float rotationSpeed;
    private Circle circle;
    public Meteor(TextureRegion img,
                  float x,
                  float y,
                  float scale,
                  float velocityX,
                  float velocityY,
                  float rotationSpeed) {
        super(img, x, y);
        this.scale = scale;
        this.velocity = new Vector2(velocityX, velocityY);
        this.rotationSpeed = rotationSpeed;
        this.circle = new Circle(x, y, img.getRegionWidth()*scale/2f);
        setOrigin(img.getRegionWidth()/2, img.getRegionHeight()/2);
    }

    public void act(float delta){
        moveBy(velocity.x * delta, velocity.y * delta);
        rotateBy(rotationSpeed * delta);
        if (getY()+getHeight()<0)
            this.remove();
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), scale, scale, getRotation());
        batch.end();
        ShapeRenderer shape = new ShapeRenderer();
        shape.setProjectionMatrix(batch.getProjectionMatrix());
        shape.begin(ShapeRenderer.ShapeType.Line);
        shape.circle(getCircle().x, getCircle().y, getCircle().radius);
        shape.end();
        batch.begin();
    }

    public Circle getCircle() {
        return new Circle(getX() + getWidth() / 2, getY() + getHeight()/2, getWidth()*scale / 2);
    }
}