package com.ads.darklands.Model;

import com.ads.darklands.View.ImageActor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by user on 2/28/2016.
 */
public class Bricks extends ImageActor {
    Body physicsBody;

    public Bricks(TextureRegion img, float x, float y, float width, float height, World world, BrickType brickType) {
        super(img, x, y, width, height);
        setName("brick");
        /*
        setSize(getWidth()-2, getHeight()-2);
        moveBy(1,1);
        */
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.fixedRotation = true;
        bodyDef.position.set(getX(), getY());
        FixtureDef fixtureDef = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        switch (brickType) {
            case FULL:
                shape.set(new Vector2[]{
                        new Vector2(0,0),
                        new Vector2(getWidth(), 0),
                        new Vector2(getWidth(), getHeight()),
                        new Vector2(0, getHeight()),
                });
                break;
            case DOWN:
                shape.set(new Vector2[]{
                        new Vector2(0,0),
                        new Vector2(getWidth(), 0),
                        new Vector2(0, getHeight()),
                });
                break;
            case UP:
                shape.set(new Vector2[]{
                        new Vector2(0,0),
                        new Vector2(getWidth(), 0),
                        new Vector2(getWidth(), getHeight()),
                });
                break;
            case RIGHT_CUT:
                shape.set(new Vector2[]{
                        new Vector2(getWidth(), getHeight()),
                        new Vector2(getWidth(),getHeight()-2),
                        new Vector2(getWidth()-2 , getHeight()-2),
                        new Vector2(getWidth()-2 , getHeight())
                });
                break;
            case LEFT_CUT:
                shape.set(new Vector2[]{
                        new Vector2(0,getHeight()),
                        new Vector2(0,getHeight()-2),
                        new Vector2(2 , getHeight()-2),
                        new Vector2(2 , getHeight())
                });
                break;
            case NONE:
                break;
        }
        fixtureDef.shape = shape;
        fixtureDef.density = 0.1f;
        fixtureDef.friction = 1.0f;
        physicsBody = world.createBody(bodyDef);
        physicsBody.createFixture(fixtureDef);
        physicsBody.getFixtureList().get(0).setUserData(this);
    }

    public Bricks(TextureRegion img, float x, float y, float width, float height) {
        super(img, x, y, width, height);
    }

    public Bricks(Texture img, float x, float y) {
        super(img, x, y);
    }

    public Bricks(Texture img, float x, float y, float width, float height) {
        super(img, x, y, width, height);
    }

    public Bricks(TextureRegion img, float x, float y) {
        super(img, x, y);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
        /*
        batch.end();
        ShapeRenderer shape = new ShapeRenderer();
        shape.setProjectionMatrix(batch.getProjectionMatrix());
        shape.begin(ShapeRenderer.ShapeType.Line);
        shape.rect(getX(),getY(),getWidth(),getHeight());
        shape.end();
        batch.begin();*/
    }

    public Rectangle getRectangle() {
        return new Rectangle(getX(), getY(), getWidth(), getHeight());
    }
}
